namespace StudentAdministrationSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelUpdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Students", "Name", c => c.String(nullable: false, maxLength: 64));
            AlterColumn("dbo.Subjects", "Name", c => c.String(maxLength: 64));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Subjects", "Name", c => c.String());
            AlterColumn("dbo.Students", "Name", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
