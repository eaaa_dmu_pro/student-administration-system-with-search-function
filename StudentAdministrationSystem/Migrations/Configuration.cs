namespace StudentAdministrationSystem.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<StudentAdministrationSystem.Storage.StudentAdminDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(StudentAdministrationSystem.Storage.StudentAdminDb context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Students.AddOrUpdate(
                s => s.Name,
                new Student { Name = "Frej Kragh Andreasen", Email = "eaafrand6@students.eaaa.dk" },
                new Student { Name = "Mikkel Hagen Bach", Email = "eaamibac1@students.eaaa.dk" },
                new Student { Name = "Daniel N�rrevang Bach", Email = "eaadabec1@students.eaaa.dk" },
                new Student { Name = "Anders Herold Christensen", Email = "eaaanchr42@students.eaaa.dk" },
                new Student { Name = "Rasmus S�e Christensen", Email = "eaarachr6@students.eaaa.dk" },
                new Student { Name = "Rune Christensen", Email = "eaaruchr5@students.eaaa.dk" },
                new Student { Name = "Joachim Danielsen", Email = "eaajodan@students.eaaa.dk" },
                new Student { Name = "Mathias Valdemar Degenkamp", Email = "eaamadeg2@students.eaaa.dk" },
                new Student { Name = "Kasper Lungs� Gimbel", Email = "eaakagim@students.eaaa.dk" },
                new Student { Name = "Morten Ejstrup Gredal", Email = "eaamogre2@students.eaaa.dk" },
                new Student { Name = "Anders Gr�n", Email = "eaaangro4@students.eaaa.dk" },
                new Student { Name = "Nikolai H�wisch Hansen", Email = "eaanihan18@students.eaaa.dk" }

                );

            context.Subjects.AddOrUpdate(
                s => s.Name,
                new Subject { Name = "C# and .Net" },
                new Subject { Name = "Database" },
                new Subject { Name = "Algorithms and Data Structures" },
                new Subject { Name = "Frontend" },
                new Subject { Name = "iOS programming" },
                new Subject { Name = "IT-Sikkerhed" }
                );

        }
    }
}
