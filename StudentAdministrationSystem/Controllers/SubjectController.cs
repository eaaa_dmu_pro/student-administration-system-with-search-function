﻿using StudentAdministrationSystem.Models;
using StudentAdministrationSystem.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentAdministrationSystem.Controllers
{
    public class SubjectController : Controller
    {
        private StudentAdminDb studentAdminDb = new StudentAdminDb();

        // GET: Subject
        public ActionResult Index()
        {
            return View(studentAdminDb.Subjects.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Subject subject)
        {
            studentAdminDb.Subjects.Add(subject);
            studentAdminDb.SaveChanges();
            return RedirectToAction("Index", studentAdminDb.Subjects.ToList());
        }

        public ActionResult Details(int id)
        {
            return View(studentAdminDb.GetSubject(id));
        }


    }
}