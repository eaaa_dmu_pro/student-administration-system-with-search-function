﻿using StudentAdministrationSystem.Models;
using StudentAdministrationSystem.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentAdministrationSystem.Controllers
{
    public class StudentController : Controller
    {
        private StudentAdminDb studentAdminDb = new StudentAdminDb();

        // GET: Student
        public ActionResult Index(string searchTerm = null)
        {
            var model = studentAdminDb.Students.OrderByDescending(s => s.Name).
                Where(s => searchTerm == null || s.Name.Contains(searchTerm)).Select(s => s);
            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Student student)
        {
            studentAdminDb.Students.Add(student);
            studentAdminDb.SaveChanges();
            return View("Index", studentAdminDb.Students.ToList());
        } 

        public ActionResult Details(int id)
        {
            return View(studentAdminDb.GetStudent(id));
        }

        public ActionResult Enroll(int id)
        {
            EnrollStudentViewModel enrollStudent = new EnrollStudentViewModel();
            enrollStudent.Student = studentAdminDb.GetStudent(id);
            enrollStudent.Subjects = studentAdminDb.GetAllSubjectsBut(enrollStudent.Student.Subjects);
            return View(enrollStudent);
        }

        [HttpPost]
        public ActionResult Enroll(EnrollStudentViewModel enrollStudent)
        {
            Student student = studentAdminDb.GetStudent(enrollStudent.Student.StudentId);
            enrollStudent.SelectedSubject = studentAdminDb.GetSubject(enrollStudent.SelectedValue);
            student.Subjects.Add(enrollStudent.SelectedSubject);
            enrollStudent.Student = student;
            studentAdminDb.SaveChanges();
            return View("EnrollDetails",enrollStudent);
        }
    }
}