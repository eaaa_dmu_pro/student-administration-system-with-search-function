﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentAdministrationSystem.Models
{
    public class EnrollStudentViewModel
    {
        public Student Student { get; set; }
        public List<Subject> Subjects { get; set; }
        public int SelectedValue { get; set; }
        public Subject SelectedSubject { get; set; }
    }
}