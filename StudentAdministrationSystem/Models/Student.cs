﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StudentAdministrationSystem.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        [Required]
        [StringLength(64)]
        [DisplayName("Navn")]
        public string Name { get; set; }
        [DisplayName("e-mail adresse")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public virtual List<Subject> Subjects { get; set; }

    }
}