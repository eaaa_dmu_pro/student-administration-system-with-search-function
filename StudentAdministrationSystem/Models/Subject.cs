﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StudentAdministrationSystem.Models
{
    public class Subject
    {
        public int SubjectId { get; set; }
        [DisplayName("Fag")]
        [StringLength(64)]
        public string Name { get; set; }
        [DisplayName("Fag beskrivelse")]
        [DataType(DataType.Text)]
        public string Description { get; set; }
        public virtual List<Student> Students { get; set; }
    }
}