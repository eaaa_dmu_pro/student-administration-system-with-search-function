﻿using StudentAdministrationSystem.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StudentAdministrationSystem.Storage
{
    public class StudentAdminDb : DbContext
    {
        public StudentAdminDb() : base("name=StudentAdminDb")
        {

        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }

        public Student GetStudent(int id)
        {
            return Students.Where(s => s.StudentId == id).First();
        }

        public List<Subject> GetAllSubjectsBut(List<Subject> subjects)
        {
            return Subjects.ToList().Except(subjects).ToList();
        }

        public Subject GetSubject(int id)
        {
            return Subjects.Where(s => s.SubjectId == id).First();
        }
    }
}